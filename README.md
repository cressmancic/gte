# The Goldtoken Enhancements Project #

## About ##

This project is a Mozilla Firefox Add-on that adds enhancements to the user interface at goldtoken.com.
I have created it for my personal use, but invite any other players on the site to use it if they wish.
I also invite goldtoken's developers to borrow anything I have done
and incorporate it in their code base (this is why I have made this repository public).

## DISCLAIMER / TERMS OF USE ##

This add-on is provided on an AS IS basis.

I do NOT gaurantee that the enhancements I have made will actually work.
After clicking on "end this move" you should review carefully to make sure
that you are submitting the move you think you are.

If you use this add-on and like it and want more, please consider a donation.
You can donate golderos to me (I am "Quantum").

## Enhancements ##

### Xiphias Family of Games ###

Instead of clicking to build an alien and then click to move it and then click the spot to move it to, just click on the spot adjacent to a planet you own that you wish to move to and the program will determine how to build or move existing aliens to get there.

In the top row of the gameboard, the controls have been added for force fields and rockets. Click on the symbol to enter into alternative build modes.
For example, click the force field icon to enter forcefield building mode.
Then click one or more spots to build a force field.
Finally, click the force field icon to return to normal build mode.

The interface for merging regions is rather unfriendly at this time, but the way it works is that when multiple regions are able to attack the planet that you click, a popup will ask you to enter 1, 2 or 3 (or most commonly just 1 or 2). Start at the planet directly above the one you clicked on and go around clockwise. Label your own regions that you come to in that order with 1, 2, and 3 if applicable and that is the region from which you will make the attack.

Currently the best way to undo is to click "end your turn", then edit the URL in the address bar of your browser and then press enter. Ex. Press Ctrl+L to select the address bar, press Right Arrow to go to the end, backspace the "x" which means end the turn, then backspace back to the most recent "m" which is the most recent move - or the most recent "a" which is the most recent Alien addition, and finally press enter to reload the page at that point in the move history.

Xiphiasyn will not work, but I hope to add support eventually.

