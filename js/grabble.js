var TD_STYLE = {height: '24px', width: '20px'};

$(document).ready(function() {
    var titleTag = document.getElementsByTagName('title')[0].text;

    // Only run on Grabble or Crossdowns
    if (titleTag.indexOf("Grabble") < 0 && titleTag.indexOf("Crossdowns") < 0) {
        return false;
    }

    // Get the table containing the board elements
    var gameboard = $("div#board");
    if (gameboard.length == 0) {
        // This would happen when selecting color at the beginning, and possibly a couple of other edge cases
        // regardless, if the div does not exist, then there is nothing to do here.
        return false;
    }

    // Count the letters in use
    var usedLetters = {};
    $("div#board, div#score").find("td img").each(function () {
        var l = $(this).attr("title");
        // exception is that wilds can only be identified by the fact that the src attr ends in "dS.gif"
        if ($(this).attr("src").endsWith("dS.gif")) {
            l = "wild";
        }
        if (!usedLetters.hasOwnProperty(l)) {
            usedLetters[l] = 0;
        }
        usedLetters[l] ++;
    });

    // Provide remaining letter count in the help section
    var letCount;
    $("div#long").find("td").each(function() {
        // All tile count cells have align right
        if ($(this).attr("align") == "right") {
            if ($(this).text().length > 0) {
                // The text will be `<int> x `
                letCount = parseInt($(this).text());
            }
            else {
                // When the letter count is one, then it is blank
                letCount = 1;
            }
        }
        // All letter identifier cells have images with title text
        if ($(this).children("img").length > 0) {
            var thisLetter = $(this).children().attr("title");
            var numLeft = letCount;
            if (usedLetters.hasOwnProperty(thisLetter)) {
                numLeft -= usedLetters[thisLetter];
            }
            if (numLeft > 0) {
                var numLeftSpan = $("<span>").text(" " + numLeft).css("color", "red");
                $(this).append(numLeftSpan);
            }
        }
    });
});

